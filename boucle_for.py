#!/usr/bin/python3
""" ce script test la boucle FOR """

# constantes
WEEK_DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
LA_SEMAINE = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

def lance_traduction():
    """ Traduit les jours de la semaine """
    jour_a_traduire = input("Donner un jour>")
    jour_en_anglais = traduit_jour(jour_a_traduire)
    print(jour_a_traduire + " => " + jour_en_anglais)

def traduit_jour(le_jour):
    """ Traduit un jour en anglais """
    for index in range(len(LA_SEMAINE)):
        if le_jour == LA_SEMAINE[index]:
            return WEEK_DAYS[index]
    return "Non trouve"

if __name__ == "__main__":
    lance_traduction()
